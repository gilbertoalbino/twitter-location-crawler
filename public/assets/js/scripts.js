(function ($) {
  $(document).on('submit', 'form', function (event) {

    event.preventDefault();
    event.stopImmediatePropagation();

    if ($('#searchField').val() == '') {
      $('#searchField').focus();
      return false;
    }

    $('#loading').show();
    $('#responseData, #errorMessage').hide();

    $.when($.ajax({
      type: 'POST',
      url: '/api/crawl',
      data: {username: $('#searchField').val()}
    }).done(function (response) {
      $('#loading').hide();
      $('#responseData').html(response).show();
      initMap();
    }).fail(function (response) {
      $('#loading, #responseData, #map').hide();
      $('#errorMessage').html(response.responseJSON.error).show();
      $('#searchField').focus();
    }));
  });
})(jQuery);