<?php
/**
 * @link                   https://bitbucket.org/gilbertoalbino/twitter-location-crawler for the canonical source repository
 * @copyright       Copyright (c) 2018 Gilberto Albino (http://www.gilberto.com)
 * @license             Not Applied
 *
 */

declare(strict_types=1);

namespace App\Handler;

use App\Crawler\GoogleMapsScrapperInterface;
use App\Crawler\TwitterDomScrapperInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Template;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;

/**
 * Class ScrapHandler is used to Handle requests to the Twitter
 * username public location.
 *
 * @package App\Handler
 */
class ScrapHandler implements RequestHandlerInterface
{
    /**
     * @var GoogleMapsScrapperInterface  The Google Maps Scrapper.
     */
    private $googleMapsScrapper;

    /**
     * @var TwitterDomScrapperInterface The Twitter Dom Scrapper.
     */
    private $twitterDomScrapper;

    /**
     * @var Template\TemplateRendererInterface
     */
    private $template;

    /**
     * ScrapHandler constructor.
     * Injects the Google Maps and Twitter DOM Scrappers.
     *
     * @param GoogleMapsScrapperInterface $googleMapsScrapper
     * @param TwitterDomScrapperInterface $twitterDomScrapper
     * @param Template\TemplateRendererInterface|null $template
     */
    public function __construct(
        GoogleMapsScrapperInterface $googleMapsScrapper,
        TwitterDomScrapperInterface $twitterDomScrapper,
        Template\TemplateRendererInterface $template = null
    )
    {
        $this->googleMapsScrapper = $googleMapsScrapper;
        $this->twitterDomScrapper = $twitterDomScrapper;
        $this->template = $template;
    }

    /**
     * Handle the requested data.
     *
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {

        $error = null;
        $latLong = false;
        $latitude = 0;
        $longitude = 0;
        $address = null;
        $googleMapsLink = null;

        $username = trim($request->getParsedBody()['username']);

        $dom = $this->twitterDomScrapper->collect($username);

        if ($dom['notFound']) {
            $error = 'Twitter Username not found';
        }

        if (is_null($dom['notFound']) && !$dom['location']) {
            $error = 'User Location not publicly available.';
        }

        if (is_null($error)) {
            $latLong = $this->googleMapsScrapper->collect($dom['location']);
        }

        if ($latLong) {

            $latitude = $latLong['latitude'] ? $latLong['latitude'] : false;
            $longitude = $latLong['longitude'] ? $latLong['longitude'] : false;
            $address = $latLong['address'];

            $googleMapsLink = 'https://www.google.com/maps/place/' . $address;
        }

        if (!$latitude && !$longitude && $latLong) {
            $error = 'Could not retrieve a Google Map Location. You could try again!';
        }

        $data = [
            'error' => $error,
            'layout' => false,
            'username' => $username,
            'placeName' => $address,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'googleMapsLink' => $googleMapsLink
        ];

        if (!is_null($error)) {
            return new JsonResponse($data, 404);
        }

        return new HtmlResponse($this->template->render('app::response-data', $data));
    }
}
