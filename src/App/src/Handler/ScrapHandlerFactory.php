<?php
/**
 * @link                   https://bitbucket.org/gilbertoalbino/twitter-location-crawler for the canonical source repository
 * @copyright       Copyright (c) 2018 Gilberto Albino (http://www.gilberto.com)
 * @license             Not Applied
 *
 */
declare(strict_types=1);

namespace App\Handler;

use App\Crawler\GoogleMapsScrapperInterface;
use App\Crawler\TwitterDomScrapperInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Expressive\Template\TemplateRendererInterface;

/**
 * Class ScrapHandlerFactory handles the
 * injection of dependencies fot the Scrap Handler Action
 * @package App\Handler
 */
class ScrapHandlerFactory
{
    /**
     * All the dependencies required by ScrapHandler constructor.
     *
     * @param ContainerInterface $container
     * @return RequestHandlerInterface
     */
    public function __invoke(ContainerInterface $container): RequestHandlerInterface
    {
        $googleMapsScrapper = $container->get(GoogleMapsScrapperInterface::class);
        $twitterDomScrapper = $container->get(TwitterDomScrapperInterface::class);
        $template = $container->has(TemplateRendererInterface::class)
            ? $container->get(TemplateRendererInterface::class)
            : null;

        return new ScrapHandler($googleMapsScrapper, $twitterDomScrapper, $template);
    }
}
