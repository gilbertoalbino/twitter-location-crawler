<?php
/**
 * @link                   https://bitbucket.org/gilbertoalbino/twitter-location-crawler for the canonical source repository
 * @copyright       Copyright (c) 2018 Gilberto Albino (http://www.gilberto.com)
 * @license             Not Applied
 *
 */

namespace App\Crawler;

/**
 * Interface GoogleMapsScrapperInterface
 * bridges between  dedicated ScrapperInterface for GoogleMapsScrapper.
 *
 * @package App\Crawler
 */
interface GoogleMapsScrapperInterface extends ScrapperInterface
{
}