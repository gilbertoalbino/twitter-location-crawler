<?php
/**
 * @link                   https://bitbucket.org/gilbertoalbino/twitter-location-crawler for the canonical source repository
 * @copyright       Copyright (c) 2018 Gilberto Albino (http://www.gilberto.com)
 * @license             Not Applied
 *
 */

namespace App\Crawler;

use Throwable;

/**
 * Class InvalidMapAddressArgumentException
 * Triggers a message informing the argument
 * required by GoogleMapsScrapper is null.
 *
 * @package App\Crawler
 */
class InvalidMapAddressArgumentException extends \Exception
{
    public function __construct(int $code = 0, Throwable $previous = null)
    {
        $message = sprintf('The GoogleMapsScrapper::collect() requires an address string');
        parent::__construct($message, $code, $previous);
    }
}