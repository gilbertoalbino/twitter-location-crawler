<?php
/**
 * @link                   https://bitbucket.org/gilbertoalbino/twitter-location-crawler for the canonical source repository
 * @copyright       Copyright (c) 2018 Gilberto Albino (http://www.gilberto.com)
 * @license             Not Applied
 *
 */

namespace App\Crawler;

/**
 * Interface ScrapperInterface
 *
 * Collects data for Crawler Scrappers.
 *
 * @package App\Crawler
 */
interface ScrapperInterface
{
    /**
     * Collect the data to be parsed.
     *
     * @param $data
     * @return mixed
     */
    public function collect($data);

    /**
     * Return the parsed data.
     *
     * @return mixed
     */
    public function getData();
}