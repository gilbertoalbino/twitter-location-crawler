<?php
/**
 * @link                   https://bitbucket.org/gilbertoalbino/twitter-location-crawler for the canonical source repository
 * @copyright       Copyright (c) 2018 Gilberto Albino (http://www.gilberto.com)
 * @license             Not Applied
 *
 */

namespace App\Crawler;

/**
 * Class AbstractGoogleMapsScrapper
 * collects and parse the location data.
 *
 * @package App\Crawler
 */
abstract class AbstractGoogleMapsScrapper implements GoogleMapsScrapperInterface
{
    /*
     * Human readable address.
     *
     * @var string.
     */
    public $address;

    /**
     * Comma-separed latitude and longitude address.
     *
     * @var string
     */
    public $latLongAddress;

    /**
     * The flag for lat-long address type.
     *
     * @var boolean
     */
    public $isLatLongAddress;

    /**
     * The address latitude.
     *
     * @var float
     */
    public $latitude;

    /**
     * The address longitude.
     *
     * @var longitude.
     */
    public $longitude;

    /**
     * The Google Maps API key.
     *
     * @var string
     */
    public $key = 'AIzaSyDSUmJPWaQ4KO98AHvWjM9XUqbIe273N7Y';

    /**
     * The Google Maps Request URL.
     *
     * @var string
     */
    public $url = 'https://maps.googleapis.com/maps/api/geocode/json';

    /**
     * Collect the address to be parsed.
     *
     * @param $address The address to be parsed.
     * @return array|bool|mixed
     * @throws InvalidMapAddressArgumentException
     */
    public function collect($address)
    {
        if (empty($address)) {
            throw new InvalidMapAddressArgumentException();
        }

        $this->parseLatLongAddress($address);

        return $this->getData();
    }

    /**
     * Parse the address provided by collect().
     * It will prepare the addresses to be used later on.
     *
     * @param $address
     * @return bool
     */
    public function parseLatLongAddress($address)
    {

        $sanitizedAddress = preg_replace("/([^-\.,0-9])/", '', $address);
        $addressParts = explode(',', $sanitizedAddress);

        $foundLatLong = 0;

        foreach ($addressParts as $part) {
            if (strlen($part) > 0) {
                $foundLatLong++;
            }
        }

        $this->address = str_replace(' ', '+', $address);
        $this->latLongAddress = join(',', $addressParts);

        return $this->isLatLongAddress = ($foundLatLong > 0);
    }

    /**
     * Helper method to the getUrl() returns the type of request.
     *
     * @return string
     */
    public function getUrlType()
    {
        return $this->isLatLongAddress ? 'latlng' : 'address';
    }

    /**
     * Helper method to the getUrl() returns the address.
     * @return mixed
     */
    public function getUrlAddress()
    {
        return $this->isLatLongAddress
            ? $this->latLongAddress
            : $this->address;
    }

    /**
     * Helper method to the getUrl() returns the google maps api key.
     *
     * @return string
     */
    public function getUrlKey()
    {
        return $this->key;
    }

    /**
     * Return the Url needed by Google Maps API.
     *
     * @return string
     */
    public function getUrl()
    {
        return sprintf(
            '%s?%s=%s&sensor=false&key=%s',
            $this->url,
            $this->getUrlType(),
            $this->getUrlAddress(),
            $this->getUrlKey()
        );
    }

    /**
     * All the scrapped data is here.
     *
     * @return array|bool|mixed
     */
    public function getData()
    {
        $data = [];

        $contents = json_decode(file_get_contents($this->getUrl()));

        if ($contents->status == 'OK') {

            $results = $contents->results[0];

            $data['address'] = $results->formatted_address;
            $data['latitude'] = $results->geometry->location->lat;
            $data['longitude'] = $results->geometry->location->lng;
        }

        return (!empty($data)) ? $data : false;
    }
}