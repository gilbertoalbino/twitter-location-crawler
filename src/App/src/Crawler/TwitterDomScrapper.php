<?php
/**
 * @link                   https://bitbucket.org/gilbertoalbino/twitter-location-crawler for the canonical source repository
 * @copyright       Copyright (c) 2018 Gilberto Albino (http://www.gilberto.com)
 * @license             Not Applied
 *
 */

namespace App\Crawler;

/**
 * Class TwitterDomScrapper
 * share the GoobleMapsScrapper to the application.
 *
 * @package App\Crawler
 */
final class TwitterDomScrapper extends AbstractTwitterDomScrapper
{

}