<?php
/**
 * @link                   https://bitbucket.org/gilbertoalbino/twitter-location-crawler for the canonical source repository
 * @copyright       Copyright (c) 2018 Gilberto Albino (http://www.gilberto.com)
 * @license             Not Applied
 *
 */

namespace App\Crawler;

use Psr\Container\ContainerInterface;

/**
 * Class TwitterDomScrapperFactory
 * handles the injection of dependencies required by TwitterDomScrapper.
 *
 * @package App\Crawler
 */
class TwitterDomScrapperFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $configs = [];
        return new TwitterDomScrapper($configs);
    }
}