<?php
/**
 * @link                   https://bitbucket.org/gilbertoalbino/twitter-location-crawler for the canonical source repository
 * @copyright       Copyright (c) 2018 Gilberto Albino (http://www.gilberto.com)
 * @license             Not Applied
 *
 */

namespace App\Crawler;

use Zend\Dom\Query;
use Zend\Http\Client;
use Zend\Http\Client\Adapter\Curl;

/**
 * Class AbstractTwitterDomScrapper
 * collects and parse the user account location.
 *
 * @package App\Crawler
 */
abstract class AbstractTwitterDomScrapper implements TwitterDomScrapperInterface
{
    /**
     * The Twitter account username.
     *
     * @var string
     */
    protected $username;

    /**
     * Not Found 404 flag.
     *
     * @var boolean
     */
    protected $notFound;

    /**
     * The account public location.
     *
     * @var string
     */
    protected $location;

    /**
     * Collected the username to be parsed.
     *
     * @param $username
     * @return array|mixed
     * @throws \Exception
     */
    public function collect($username)
    {
        if (empty($username)) {
            throw new \Exception();
        }

        $this->parseIsValidUsername($username);

        return $this->getData();
    }

    /**
     * Parse the username account.
     * It wil provide a a notFound 404 validation.
     *
     * @param $username
     */
    public function parseIsValidUsername($username)
    {
        $client = new Client();
        $client->setAdapter(Curl::class);
        $client->setUri('https://twitter.com/' . $username);
        $request = $client->send();
        $html = $request->getBody();

        $dom = new Query($html);
        $search = $dom->execute('.ProfileHeaderCard-locationText');

        $this->notFound = $dom->execute('.search-404')->current();
        $this->location = ($search->current()) ? trim($search->current()->nodeValue) : false;
    }

    /**
     * All the scrapped data is here.
     *
     * @return array|bool|mixed
     */
    public function getData()
    {
        return [
            'username' => $this->username,
            'notFound' => $this->notFound,
            'location' => $this->location,
        ];
    }
}